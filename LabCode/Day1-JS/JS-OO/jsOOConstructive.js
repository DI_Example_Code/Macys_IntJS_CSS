
function Person(theName, theAge, theGender) {
	this.name = theName;
	this.age = theAge;
	this.gender = theGender;

	this.sayHello = function() {
		//	We CAN access this here,
		//	because we are within the instance
		_printGreeting();
	}

	// This is PRIVATE (a function defined within a function)
	function _printGreeting() {
		console.log("Hello! My name is " + this.name + ".");
	}
}

var student = new Person("CB", 7, "Male");

student.sayHello();  // Works
student._printGreeting();	// Fails

