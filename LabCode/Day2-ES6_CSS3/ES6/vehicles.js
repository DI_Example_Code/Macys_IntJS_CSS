class Vehicle {
	constructor(numWheels, numDoors, maxSpeed, initialMileage) {
		this.wheels = numWheels;
		this.doors = numDoors;
		this.maximumSpeed = maxSpeed;
		this.mileage = initialMileage;
	}

	honk() {
		console.log("I am a " + this.constructor.name + " - HONK");

		// return this;
	}

	drive(distance) {
		this.mileage += distance ? distance : this.maximumSpeed;

		return this;
	}
}

//	Create a Vehicle
//	check type
//	check instance


class Car extends Vehicle {
	constructor(theMake, theModel, aColor, wheels=4, doors=4, topSpeed=140, miles=25) {
		super(wheels, doors, topSpeed, miles);

		this.make = theMake;
		this.model = theModel;
		this.color = aColor;
	}

	honk() {
		super.honk();
		console.log("Car says BEEP-BEEP!");

		return this;
	}
}

class Bus extends Vehicle {
	constructor(length=Bus.BusType.Standard, wheels=6, doors=2, topSpeed=80, miles=60000) {
		super(wheels, doors, topSpeed, miles);

		this.busLength = length;
	}

	honk() {
		console.log("Bus says BRONK! BRONK!!");

		return this;
	}

	drive(distance) {
		return super.drive(distance * 2);
	}
}

Bus.BusType = {
	Short: 16,
	Standard: 29,
	Super: 36
}

class Bike extends Vehicle {

	constructor(brand, wheels=2, doors=0, topSpeed=35, miles=2) {
		super(wheels, doors, topSpeed, miles);

		this.manufacturer = brand;
	}

	honk() {
		console.log("Bike says RING, RING...");

		return this;
	}

	drive(distance=10) {
		return super.drive(distance);
	}
}