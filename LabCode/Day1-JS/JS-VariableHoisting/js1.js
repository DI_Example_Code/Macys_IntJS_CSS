// Problem 1:
console.log(first_variable);
var first_variable = "Yipee I was first!";
function firstFunc() {
  first_variable = "Not anymore!!!";
  console.log(first_variable);
}
console.log(first_variable);

// Solution:
// var first_variable; // Variable declaration gets hoisted (undefined)
// function firstFunc() { // Standalone function gets hoisted (never called..)
//   first_variable = "Not anymore!!!";
//   console.log(first_variable);
// }
// console.log(first_variable); // ----------> undefined
// first_variable = "Yipee I was first!"; // Global var gets set for the first time
// console.log(first_variable); // ----------> "Yipee I was first!"


