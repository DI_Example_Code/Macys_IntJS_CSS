function celebrityName(firstName) {
	var nameIntro = "This celebrity is ";

	// this inner function has access to the outer function's variables, including the parameter​
	function lastName(theLastName) {
        return nameIntro + firstName + " " + theLastName;
    }

    return lastName;
}
​
​var celebNameFunc = celebrityName("Johnny"); // At this juncture, the celebrityName outer function has returned.​
​
​// The closure (lastName) is called here after the outer function has returned above​,
​// but the closure still has access to the outer function's variables and parameter​
celebNameFunc("Depp"); // Prints 'This celebrity is Johnny Depp'