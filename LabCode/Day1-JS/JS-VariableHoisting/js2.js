// Problem 2:
var food = "Chicken";
function eat() {
  food = "half-chicken";
  console.log(food);
  var food = "gone";       // CAREFUL!
  console.log(food);
}
eat();
console.log(food);

// Solution:
// var food; // undefined
// function eat() {
//   var food; // undefined
//   food = "half-chicken"; // changes local variable food to "half-chicken"
//   console.log(food); // -----------> "half-chicken"
//   food = "gone";  // changes local variable food to "gone"
//   console.log(food); // -----------> "gone"
// }
// food = "Chicken";
// eat(); // runs the function
// console.log(food); // -----------> "Chicken"