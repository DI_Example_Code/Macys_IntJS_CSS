1. What will this code produce:

$(".important").wrap("<H1>")




2. Given this:

<div id="expander"></div>

#expander {
  width: 100px;
  height: 100px;
  background-color: #C342D2;
}

Write the code to expand the div to 200x200 over the course of 2 seconds.




3. What does this code do?

$("div#first, div.first, ol#items > [name$='first']")




4. How does $.ajax(someURL, {cache: false}) work?




5. Which line of code is more efficient:

document.getElementById("logo");
 - or -
$("#logo");




6. Given this:
<ol>
	<li>Item 1</li>
	<li>Item 2</li>
	<li>Item 3</li>
	<li>Item 4</li>
	<li>Item 5</li>
</ol>

What does this do:
$("ol").append($("li").eq(2))




7. Explain the difference between stopPropagation() and preventDefault()
 
