// Problem 3:
var new_word = "NEW!";
function lastFunc() {
  new_word = "old";
}
console.log(new_word);


// Solution:
// var new_word; // undefined
// function lastFunc() { // never called
//   new_word = "old";
// }
// new_word = "NEW!";
// console.log(new_word); // -------------> "NEW!"
