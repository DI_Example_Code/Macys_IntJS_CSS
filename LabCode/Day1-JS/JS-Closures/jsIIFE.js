
//	#1	Consider:
function sayHi() {
	alert("Hello!");
}

sayHi();



//	#2	Create a function expression (in this case a lambda)
var msg = "Hello!";
var sayHi = function() {
    alert(msg);
};

sayHi();



//	#3	Function expressions can have names
var fibo = function fibonacci() {
    // you can use "fibonacci()" here as
    // this function expression has a name.
};

// fibonacci() here fails, but fibo() works.



//	#4	IIFE variations on form
!function() {
    alert("Hello from an IIFE!");
}();

//	The unary ! forces the interpreter
//	to treat what comes after as an 
//	expression rather than a func def

void function() {
    alert("Hello from an IIFE!");
}();



//	#5	Traditional form
(function() {
    alert("OG IIFE!");
})();



//	#6	Bad form
function isThisAnIIFE() {
	// some code here
}();  // <-- ERROR!



//	#7	Privacy
(function initGame() {
    // Private variables that no one has access to outside this IIFE
    var lives;
    var weapons;
    
    init();

    // Private function that no one has access to outside this IIFE
    function init() {
        lives = 5;
        weapons = 10;
    }
})();



//	#8	Return a value
var result = (function() {
    return "IIFE says WHAT?!";
})();

alert(result);



//	#9	Parameters
(function IIFE(msg, times) {
    for (var i = 1; i <= times; i++) {
        console.log(msg);
    }
})("Hello!", 5);


//	#10	Sequence singleton

var Sequence = (function sequenceInit() {
    // Private variable to store current counter value.
    var current = 0;
    
    // Object that's returned from the IIFE.
    return {
    	getCurrentValue: function() {
            return current;
        },
        
        getNextValue: function() {
            current = current + 1;
            return current;
        }
    };  
})();

console.log(Sequence.getNextValue());
console.log(Sequence.getNextValue());
console.log(Sequence.getCurrentValue());


