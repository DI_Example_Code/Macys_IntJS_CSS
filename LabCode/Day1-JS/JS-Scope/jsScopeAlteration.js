
var lineItems = document.querySelectorAll("li");

for (var i = 0; i < lineItems.length; i++) {
	//	this is bound to the global scope
	console.log(this);	// [object Window]
}

for (var i = 0; i < lineItems.length; i++) {
	//	we can alter the context from which the
	//	console.log is called, changing its scope
	(function() {
		console.log(this);	// iterated element
	}).call(lineItems[i]);
}