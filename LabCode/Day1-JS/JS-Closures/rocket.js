
var materials = [
  'Hydrogen',
  'Helium',
  'Lithium',
  'Beryllium'
];

//	Typical
materials.map(function(material) { 
  return material.length; 
}); // [8, 6, 7, 9]


//	With rocket syntax
materials.map((material) => {
  return material.length;
}); // [8, 6, 7, 9]


//	Optimal
materials.map(material => material.length); // [8, 6, 7, 9]
