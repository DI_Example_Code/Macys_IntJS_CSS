//	THIS IS A NODEJS SCRIPT

const http = require('http');
var promises = [];
var prom;
var respArray = [];

for (var i = 2; i < process.argv.length; i++) {
	const idx = i;
	
	prom = new Promise(function(resolve, reject) {
		http.get(process.argv[i], function(resp) {
			respArray[idx - 2] = "";

			resp.setEncoding('utf8');

			resp.on('data', function(d) {
				respArray[idx - 2] += d;
			});

			resp.on('end', function() {
				resolve("Request " + (idx - 2) + " complete");
			});
		}).on('error', reject);
	});

	promises.push(prom);

	prom.then(function(response) {
	  console.log("Success!", response);
	}, function(error) {
	  console.error("Failed!", error);
	});
}

Promise.all(promises).then(function(response) {
	console.log("ALL PAGES LOADED!", response);
	}, function(error) {
	  console.error("ERROR:::::", error);
	}
);
