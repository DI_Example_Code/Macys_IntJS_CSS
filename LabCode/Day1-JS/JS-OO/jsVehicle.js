function Vehicle(numWheels, numDoors, maxSpeed, initialMileage) {
	this.wheels = numWheels;
	this.doors = numDoors;
	this.maximumSpeed = maxSpeed;
	this.mileage = initialMileage;
}

Vehicle.prototype.honk = function() {
	console.log("I am a " + this.constructor.name + " - HONK");
}

Vehicle.prototype.drive = function(distance) {
	this.mileage += distance ? distance : this.maximumSpeed;
}


function Car(numWheels, numDoors, maxSpeed, initialMileage, theMake, theModel, aColor) {
	if (numWheels == undefined) numWheels = 4;
	if (numDoors == undefined) numDoors = 4;
	if (maxSpeed == undefined) maxSpeed = 140;
	if (initialMileage == undefined) mileage = 0;

	// Construct myself as a Vehicle to 'inherit' properties
	Vehicle.call(this, numWheels, numDoors, maxSpeed, initialMileage);

	this.make = theMake;
	this.model = theModel;
	this.color = aColor;
}

//	Set my prototype to be that of my superclass
Car.prototype = Object.create(Vehicle.prototype);

//	Reset my constructor, so that when we create a new instance the 'shape' is correct
Car.prototype.constructor = Car;

//	Add any instance methods
Car.prototype.honk = function() {
	Vehicle.prototype.honk.call(this);
	console.log("Beep Beep!");
}


function Bus(numWheels, numDoors, maxSpeed, initialMileage, lengthStr) {
	numWheels = numWheels || 6; // 4 at the back and 2 in front
	numDoors = numDoors || 2;	// emergency exit at back and folding at front
	maxSpeed = maxSpeed || 80;
	initialMileage = initialMileage || 0;

	// Construct myself as a Vehicle to 'inherit' properties
	Vehicle.call(this, numWheels, numDoors, maxSpeed, initialMileage);

	this.length = lengthStr;
}

//	Set my prototype to be that of my superclass
Bus.prototype = Object.create(Vehicle.prototype);

//	Reset my constructor, so that when we create a new instance the 'shape' is correct
Bus.prototype.constructor = Bus;

//	Add any instance methods
Bus.prototype.honk = function() {
	Vehicle.prototype.honk.call(this);
	console.log("BRONK! BRONK!");
}


function Bike(numWheels, numDoors, maxSpeed, initialMileage, aBrand) {
	if (numWheels == undefined) numWheels = 2;
	if (numDoors == undefined) numDoors = 0;
	if (maxSpeed == undefined) maxSpeed = 25;
	if (initialMileage == undefined) mileage = 0;

	// Construct myself as a Vehicle to 'inherit' properties
	Vehicle.call(this, numWheels, numDoors, maxSpeed, initialMileage);

	this.manufacturer = aBrand;
}

//	Set my prototype to be that of my superclass
Bike.prototype = Object.create(Vehicle.prototype);

//	Reset my constructor, so that when we create a new instance the 'shape' is correct
Bike.prototype.constructor = Bike;

//	Add any instance methods
Bike.prototype.honk = function() {
	Vehicle.prototype.honk.call(this);
	console.log("Ring, ring...");
}


