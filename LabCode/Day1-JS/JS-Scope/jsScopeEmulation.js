
function Container(aContainee) {
	//	aContainee is PRIVATE
	//	member is a PUBLICly accessible member or instance variable
	this.member = aContainee;

	//	secret and that are PRIVATE variables
	//		* that is used to make the Container object available to private methods,
	//		as a workaround for the way "this" is handled
	var secret = 3;
	var that = this;

	//	decrement() is a PRIVATE function
	function decrement() {
		if (secret > 0) {
			secret -= 1;

			return true;
		}
		else {
			return false;
		}
	}

	//	service() is a PRIVILEGED function that is publicly
	//	accessible, but capable of accessing private data
	this.service = function() {
		return decrement() ? that.member : null;
	}
}


