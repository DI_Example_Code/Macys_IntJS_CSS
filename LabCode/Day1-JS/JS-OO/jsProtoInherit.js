function Person(first, last, age, gender, interests) {
	this.name = {first, last};

	this.age = age;
	this.gender = gender;
	this.interests = interests;

	this.greeting = function() {
		alert('Hi! I\'m ' + this.name.first + '.');
	}
};


function Teacher(first, last, age, gender, interests, subject) {
	Person.call(this, first, last, age, gender, interests);

	this.subject = subject;

	this.greeting = function() {
		var prefix;

		if (this.gender.toLowerCase() == 'male' || this.gender.toLowerCase() == 'm') {
		    prefix = 'Mr.';
		}
		else if (this.gender.toLowerCase() == 'female' || this.gender.toLowerCase() == 'f') {
		    prefix = 'Mrs.';
		}
		else {
		    prefix = 'Mx.';
		}

		alert('Hello. My name is ' + prefix + ' ' + this.name.last + ', and I teach ' + this.subject + '.');
	}
};

Teacher.prototype = Object.create(Person.prototype);
Teacher.prototype.constructor = Teacher;



