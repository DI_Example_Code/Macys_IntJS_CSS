

var promise = new Promise(function(resolve, reject) {
  // do a thing, possibly async, then…

  if (/* everything turned out fine */) {
    resolve("Stuff worked!");
  }
  else {
    reject("It broke");
  }
});


promise.then(function(result) {
	//	Passed as 'resolve'
  	console.log(result); // "Stuff worked!"
}, function(err) {
	//	Passed as 'reject'
  	console.log(err); // "It broke"
});